package com.gft.qa.automation.core.utils;

public class Constant {
	public static final String Path_TestDataLogin = ".//src//test//resources//com//testdata//";
	public static final int Waiting_Seconds = 10;
	public static final int Waiting_ExtraTime_Seconds = 10;
	public static final int Waiting_For_Invisibility_Seconds = 30;
	public static final String baseURL = "";
	public static final String dateFormat = "MM/dd/yyyy";
	public static final String userDirectory = System.getProperty("user.dir");
}