package com.gft.qa.automation.core.common;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private Properties prop = new Properties();
    private InputStream input = null;
    private String FileName = "application.properties";//Default File

    public PropertiesLoader(){
        this.loadFile();
        System.out.println("Loading Properties File: "+this.getPropertyFileName());
    }

    private Boolean loadFile(){
        try{
            input = getClass().getClassLoader().getResourceAsStream(this.FileName);
            if (input == null) {
                System.out.println("Sorry, unable to find " + this.FileName);
                return false;
            }
            prop.load(input);
            return true;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    public void setPropertyFileName(String propertyFile){
        this.FileName = propertyFile;
        this.loadFile();
    }

    public String getPropertyFileName(){
        return this.FileName;
    }

    public Properties getProperties(){
        try{
            return this.prop;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String getProperty(String property){
        try{
            return this.prop.getProperty(property);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

}
