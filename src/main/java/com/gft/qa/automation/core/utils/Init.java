package com.gft.qa.automation.core.utils;

import com.gft.qa.automation.core.common.PropertiesLoader;
import com.google.common.io.Resources;
import org.apache.log4j.xml.DOMConfigurator;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Properties;

public class Init {

    public static void initEnvironmentSettings() {

        //Setting up LOG4J
        DOMConfigurator.configure(Resources.getResource("log4j.xml"));

        //Loading Environment Properties
        PropertiesLoader pl = new PropertiesLoader();
        ArrayList<Properties> properties;
        properties = new ArrayList<>();
        properties.add(pl.getProperties());

        //Setting up GUtils properties
        GUtils.properties = properties;

    }

}
