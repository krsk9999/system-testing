/*
* Created by Kenneth Ramirez Sanchez
* */

package com.gft.qa.automation.core.utils;

import com.gft.qa.automation.core.common.Filter;
import com.gft.qa.automation.core.exceptions.*;
import com.google.common.io.Resources;
import cucumber.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class GUtils {

    //<editor-fold desc="Variables">

    public static WebDriver driver = null;
    public static Scenario scenario = null;
    public static JavascriptExecutor jsExecutor = null;

    public static ArrayList<Properties> properties = new ArrayList<>();

    //</editor-fold>

    //<editor-fold desc="Browser Methods">

    public enum BrowserEnum {
        FIREFOX,
        CHROME,
        IE,
        OPERA,
        IPHONE,
        ANDROID
    }

    public static WebDriver OpenBrowser() throws Exception {

        DesiredCapabilities capabilities;

        try {
            Log.info("Starting driver configuration.");

            BrowserEnum currentBrowser = BrowserEnum.valueOf(properties.get(0).getProperty("browser").toUpperCase());

            jsExecutor = (JavascriptExecutor) driver;

            switch (currentBrowser) {

                case FIREFOX:
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    driver.manage().window().maximize();
                    break;
                case CHROME:
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("test-type");
                    options.addArguments("start-maximized");
                    driver = new ChromeDriver(options);
                    break;
                case IE:
                    capabilities = DesiredCapabilities.internetExplorer();
                    WebDriverManager.iedriver().setup();
                    InternetExplorerOptions ieOptions = new InternetExplorerOptions(capabilities);
                    driver = new InternetExplorerDriver(ieOptions);
                    break;
                case OPERA:
                    //TODO Opera Feature
                    capabilities = DesiredCapabilities.operaBlink();
                    WebDriverManager.operadriver().setup();
                    driver = new OperaDriver(capabilities);
                    break;
                default:
                    String errorMessage = "The browser '" + currentBrowser.toString() + "' is not valid.";
                    Log.error(errorMessage);
                    throw new UnknownBrowserException(errorMessage);
            }
            maximize();
            Log.info("WebDriver has been properly instantiated: " + currentBrowser.toString());
        } catch (Exception e) {
            Log.error("Class GUtils | Method OpenBrowser | Exception desc : " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw e;
        }
        return driver;
    }

    private static void enablejQuery() throws Exception {
        try {
            if (!GUtils.isjQueryLoaded()) {
                URL jqueryUrl = Resources.getResource("jquery-3.3.1.min.js");
                String jqueryText = Resources.toString(jqueryUrl, Charsets.UTF_8);
                jsExecutor.executeScript(jqueryText);
            }
        } catch (Exception ex) {
            Log.error("There was an error while enabling jQuery.");
            throw ex;
        }
    }

    private static Boolean isjQueryLoaded() throws Exception {
        Boolean result;
        try {
            result = (Boolean) jsExecutor.executeScript("return !!window.jQuery;");
        } catch (Exception ex) {
            Log.error("There was an error while loading jQuery.");
            throw ex;
        }
        return result;
    }

    public static Object executeJquery(final String js) throws Exception {
        try {
            GUtils.enablejQuery();
            return GUtils.jsExecutor.executeScript(String.format("return %s", js));
        } catch (Exception ex) {
            GUtils.captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static Object executeJqueryWhitinElement(final String js, final WebElement element) throws Exception {
        try {
            GUtils.enablejQuery();
            return GUtils.jsExecutor.executeScript(String.format("return %s", js), element);
        } catch (Exception ex) {
            GUtils.captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    //</editor-fold>

    //<editor-fold desc="Wait Methods">

    public static WebElement waitForElementToBeClickable(WebElement element, String name) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Log.info("Wait for element '" + name + "' is performed");
            return element;
        } catch (Exception ex) {
            Log.error("Class GUtils | Method waitForElementToBeClickable | Exception occurred while waiting for presence of the element located by " + name + ". "
                    + ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static WebElement waitForElementToBeClickable(By by) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
            Log.info("Wait for element '" + by.toString() + "' is performed");
            return element;
        } catch (Exception ex) {
            Log.error("Class GUtils | Method waitForElementToBeClickable | Exception occurred while waiting for presence of the element located by " + by.toString() + ". "
                    + ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static void waitForAllElementsLocatedBy(By by) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
            Log.info("Wait for all elements located in " + by.toString() + " is performed");
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method waitForAllElementsLocatedBy | Exception occurred while waiting for presence of all elements located by " + by.toString() + ". "
                    + e.getMessage());
            throw e;
        }
    }

    public static void waitForInvisibilityOfLoadingSpinner(By by) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_For_Invisibility_Seconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method waitForInvisibilityOfLoadingSpinner | Exception occurred while waiting for invisibility of an element: "
                    + by.toString() + ". Error: " + e.getMessage());
            throw e;
        }
    }

    public static void waitForInvisibilityOfElementLocated(By by) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method waitForInvisibilityOfElementLocated | Exception occurred while waiting for invisibility of an element: "
                    + by.toString() + ". Error: " + e.getMessage());
            throw e;
        }
    }

    public static void waitUntilElementIsDisplayed(By elementlocator) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(elementlocator));
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method waitForInvisibilityOfElementLocated | Exception occurred while waiting for invisibility of an element: "
                    + elementlocator.toString() + ". Error: " + e.getMessage());
            throw e;
        }
    }

    public static void waitUntilElementIsEnabled(final By elementlocator) throws Exception {
        try {
            new WebDriverWait(driver, Constant.Waiting_Seconds) {
            }.until(new ExpectedCondition<Boolean>() {
                //@Override
                public Boolean apply(WebDriver driver) {
                    return (driver.findElement(elementlocator).isEnabled());
                }
            });
        } catch (Exception ex) {
            Log.error("Class GUtils | Method waitUntilElementIsEnabled | Exception occurred while waiting for of an element: "
                    + elementlocator.toString() + " to be enabled. Error: " + ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static void waitUntilInstancesEqual(final By elementLocator, final int instances) throws Exception {
        try {
            new WebDriverWait(driver, Constant.Waiting_Seconds) {
            }.until(new ExpectedCondition<Boolean>() {
                //@Override
                public Boolean apply(WebDriver driver) {
                    return (driver.findElements(elementLocator).size() == instances);
                }
            });
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static void waitUntilInstancesAreMoreThan(final By elementLocator, final int instances) throws Exception {
        try {
            new WebDriverWait(driver, Constant.Waiting_Seconds) {
            }.until(new ExpectedCondition<Boolean>() {
                //@Override
                public Boolean apply(WebDriver driver) {
                    return (driver.findElements(elementLocator).size() > instances);
                }
            });
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static void waitUntilInstancesAreMoreThan(final By elementLocator, final WebElement wParent, final int instances) throws Exception {
        try {
            new WebDriverWait(driver, Constant.Waiting_Seconds) {
            }.until(new ExpectedCondition<Boolean>() {
                //@Override
                public Boolean apply(WebDriver driver) {
                    return (wParent.findElements(elementLocator).size() > instances);
                }
            });
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw ex;
        }
    }

    public static void increaseWaitingTimeForElement() {
        driver.manage()
                .timeouts()
                .implicitlyWait(Constant.Waiting_ExtraTime_Seconds,
                        TimeUnit.SECONDS);
        Log.info("Set the implicit waiting time to "
                + Constant.Waiting_ExtraTime_Seconds + " seconds");
    }

    public static void normalizeWaitingTimeForElement() {
        driver.manage().timeouts().implicitlyWait(Constant.Waiting_Seconds, TimeUnit.SECONDS);
        Log.info("Set the implicit waiting time to " + Constant.Waiting_Seconds
                + " seconds");
    }

    //</editor-fold>

    //<editor-fold desc="Click Methods">

    public static void ClickButtonThroughtJS(WebElement element, String controlName) throws Exception {
        try {
            jsExecutor.executeScript("arguments[0].click();", element);
            Log.info("Click action through javascript is performed on '" + controlName + "' button");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void ClickButton(WebElement element, String controlName) throws Exception {
        try {
            element.click();
            Log.info("Click action is performed on '" + controlName + "' button");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void ClickButton(WebElement element) throws Exception {
        try {
            element.click();
            Log.info("Click action is performed...");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void ClickTab(WebElement element, String controlName) throws Exception {
        try {
            element.click();
            Log.info("Click action is performed on '" + controlName + "' Tab");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void DoubleClick(WebElement element) throws Exception {
        try {
            Actions action = new Actions(driver);
            //Double click
            action.moveToElement(element).doubleClick().perform();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void clickElementActions(WebElement element) throws Exception {
        try {
            Actions action = new Actions(driver);
            action.click(element).perform();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    //</editor-fold>

    //<editor-fold desc="SendKeys Methods">

    public static void SendKeys(By locator, String text) throws Exception {
        try {
            findElement(locator).sendKeys(text);
            Log.info("Sendkeys " + text);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(WebElement wElement, String text) throws Exception {
        try {
            wElement.sendKeys(text);
            Log.info("Sendkeys " + text);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void ClearInputDateField(WebElement wElement) throws Exception {
        try {
            String text = wElement.getAttribute("value").replace("/", "").replace("-", "");
            for (int i = 0; i < text.length(); i++) {
                wElement.sendKeys(Keys.BACK_SPACE);
            }
            Log.info("Clearing Field");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }


    public static void SendKeysWithClear(WebElement wElement, String text) throws Exception {
        try {
            wElement.clear();
            wElement.sendKeys(text);
            Log.info("Sendkeys with Clear " + text);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(WebElement wElement, Keys key) throws Exception {
        try {
            wElement.sendKeys(key);
            Log.info("Sendkeys " + key);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(By locator, Keys key) throws Exception {
        try {
            driver.findElement(locator).sendKeys(key);
            Log.info("Sendkeys " + key);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(int rowNumber, WebElement element, String columnName) throws Exception {
        try {
            SendKeys(rowNumber, element, columnName, columnName);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(int rowNumber, WebElement element, String excelColumnName, String elementName) throws Exception {
        try {
            String dataValue = GExcelUtils.getCellData(rowNumber, excelColumnName);
            if (!dataValue.isEmpty())
                GUtils.SendKeys(element, elementName, dataValue);
            Log.info("Sendkeys " + dataValue);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeys(WebElement element, String elementName, String dataValue) throws Exception {
        try {
            element.clear();
            element.sendKeys(dataValue);
            Log.info("'" + dataValue + "' is entered in '" + elementName + "' input");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeysThroughJS(int rowNumber, WebElement element, String columnName) throws Exception {
        try {
            SendKeysThroughJS(rowNumber, element, columnName, columnName);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeysThroughJS(int rowNumber, WebElement element, String columnName, String fieldName) throws Exception {
        try {
            String dataValue = GExcelUtils.getCellData(rowNumber, columnName);
            jsExecutor.executeScript("arguments[0].value = arguments[1]", element, dataValue);
            Log.info("'" + dataValue + "' is entered in '" + fieldName + "' input");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SendKeysThroughJS(String dataValue, WebElement element, String fieldName) throws Exception {
        try {
            jsExecutor.executeScript("arguments[0].value = arguments[1]", element, dataValue);
            Log.info("'" + dataValue + "' is entered in '" + fieldName + "' input");
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void editHTMLText(String dataValue, WebElement element) throws Exception {
        try {
            jsExecutor.executeScript("arguments[0].innerHTML = arguments[1]", element, dataValue);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }


    //</editor-fold>

    //<editor-fold desc="Check/Uncheck Methods">

    public static void check(By locator) throws Exception {
        try {
            WebElement checkBox = driver.findElement(locator);
            if (!checkBox.getAttribute("type").toLowerCase().equals("checkbox")) {
                throw new InvalidElementTypeException("This elementLocator is not a checkbox!");
            }
            if (!checkBox.isSelected()) {
                checkBox.click();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void check(WebElement checkBox) throws Exception {
        try {
            if (!checkBox.getAttribute("type").toLowerCase().equals("checkbox")) {
                throw new InvalidElementTypeException("This elementLocator is not a checkbox!");
            }
            if (!checkBox.isSelected()) {
                checkBox.click();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void uncheck(By locator) throws Exception {
        try {
            WebElement checkBox = driver.findElement(locator);
            if (!checkBox.getAttribute("type").toLowerCase().equals("checkbox")) {
                throw new InvalidElementTypeException("This elementLocator is not a checkbox!");
            }
            if (checkBox.isSelected()) {
                checkBox.click();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void uncheck(WebElement checkBox) throws Exception {
        try {
            if (!checkBox.getAttribute("type").toLowerCase().equals("checkbox")) {
                throw new InvalidElementTypeException("This elementLocator is not a checkbox!");
            }
            if (checkBox.isSelected()) {
                checkBox.click();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static boolean isChecked(By locator) throws Exception {
        try {
            WebElement checkBox = driver.findElement(locator);
            if (!checkBox.getAttribute("type").toLowerCase().equals("checkbox")) {
                throw new InvalidElementTypeException("This elementLocator is not a checkbox!");
            }
            if (checkBox.getAttribute("checked").equals("checked")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            return false;
        }
    }

    public static void Check(int rowNumber, WebElement element,
                             String columnName) throws Exception {
        try {
            Check(rowNumber, element, columnName, columnName);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void Check(int rowNumber, WebElement element,
                             String columnName, String fieldName) throws Exception {
        String dataValue = GExcelUtils.getCellData(rowNumber, columnName);
        if ("Yes".equalsIgnoreCase(dataValue)) {
            if (!element.isSelected())
                element.click();

            Log.info("It is checked '" + fieldName + "' check box");
        } else {
            if (element.isSelected())
                element.click();
            Log.info("It is not checked '" + fieldName + "' check box");
        }
    }

    //</editor-fold>

    //<editor-fold desc="SelectOption Methods">

    public static void SelectOption(int rowNumber, WebElement element, String columnName) throws Exception {
        try {
            SelectOption(rowNumber, element, columnName, columnName);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void SelectOption(int rowNumber, WebElement element, String columnName, String fieldName) throws Exception {
        Boolean founded = false;
        String fullText = "";
        String dataValue = GExcelUtils.getCellData(rowNumber, columnName);
        Select select = new Select(element);
        List<WebElement> list = select.getOptions();
        for (WebElement option : list) {
            fullText = option.getText();
            if (fullText.contains(dataValue)) {
                select.selectByVisibleText(fullText);
                founded = true;
                break;
            }
        }

        if (founded)
            Log.info(String.format("'" + fullText + "' is selected in '" + fieldName + "' drop down list", dataValue));
        else {
            Log.error("Option '" + fullText + "' is not found in '" + fieldName + "' drop down list");

            // I put this line intentionally to generate an exception.
            (new Select(element)).selectByVisibleText(dataValue);
        }
    }

    public static void selectByValue(By locator, String value) throws Exception {
        try {
            Select select = new Select(driver.findElement(locator));
            select.selectByValue(value);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void selectByValue(WebElement welement, String value) throws Exception {
        try {
            Select select = new Select(welement);
            select.selectByValue(value);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void selectByIndex(By locator, int index) throws Exception {
        try {
            Select select = new Select(driver.findElement(locator));
            select.selectByIndex(index);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void selectByIndex(WebElement element, int index) throws Exception {
        try {
            Select select = new Select(element);
            select.selectByIndex(index);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void selectByVisibleText(By locator, String text) throws Exception {
        try {
            Select select = new Select(driver.findElement(locator));
            select.selectByVisibleText(text);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void selectByVisibleText(WebElement welement, String text) throws Exception {
        try {
            Select select = new Select(welement);
            select.selectByVisibleText(text);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    //</editor-fold>

    //<editor-fold desc="Element Methods">

    public static boolean doesElementExist(By locator) throws Exception {
        try {
            if (driver.findElements(locator).size() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            return false;
        }

    }

    public static WebElement findElement(By locator) throws Exception {
        try {
            if (driver.findElements(locator).size() > 0) {
                return driver.findElement(locator);
            } else {
                return null;
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            return null;
        }
    }

    public static List<WebElement> findElements(By locator) throws Exception {
        try {
            if (driver.findElements(locator).size() > 0) {
                return driver.findElements(locator);
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.error("Class GUtils | Method Pause | Exception occurred: Exception: " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw new Exception();
        }
    }

    public static boolean isElementDisplayed(By locator) throws Exception {
        try {
            if (doesElementExist(locator)) {
                return driver.findElement(locator).isDisplayed();
            } else {
                return false;
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            return false;
        }
    }

    public static int getElementCount(By locator) throws Exception {
        try {
            List elementsFound = driver.findElements(locator);
            return elementsFound.size();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            return 0;
        }
    }

    public static Boolean ExistsElement(By by, Boolean extratime) throws Exception {
        Boolean found = false;
        try {
            if (extratime)
                increaseWaitingTimeForElement();

            WebElement element = GUtils.findElement(by);
            if (element != null)
                found = true;

            if (extratime)
                normalizeWaitingTimeForElement();

        } catch (Exception e) {
            Log.error(e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            if (extratime)
                normalizeWaitingTimeForElement();
        }
        return found;
    }

    public static void clickAndHold(By locator) throws Exception {
        try {
            WebElement webElement = driver.findElement(locator);

            if (webElement != null) {
                new Actions(driver).clickAndHold(webElement).perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void clickAndHold(WebElement webElement) throws Exception {
        try {
            if (webElement != null) {
                new Actions(driver).clickAndHold(webElement).perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void releaseElement(By locator) throws Exception {
        try {
            WebElement webElement = driver.findElement(locator);

            if (webElement != null) {
                new Actions(driver).release(webElement).perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void releaseElement(WebElement webElement) throws Exception {
        try {
            if (webElement != null) {
                new Actions(driver).release(webElement).perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void clickAt(By locator, int x, int y) throws Exception {
        try {
            WebElement webElement = driver.findElement(locator);

            if (webElement != null) {
                Actions builder = new Actions(driver);
                builder.moveToElement(webElement).moveByOffset(x, y).click().perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void clickAt(WebElement webElement, int x, int y) throws Exception {
        try {
            if (webElement != null) {
                Actions builder = new Actions(driver);
                builder.moveToElement(webElement).moveByOffset(x, y).click().perform();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void submitElement(By locator) throws Exception {
        try {
            WebElement webElement = driver.findElement(locator);

            if (webElement != null) {
                webElement.submit();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static void submitElement(WebElement webElement) throws Exception {
        try {
            if (webElement != null) {
                webElement.submit();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
        }
    }

    public static WebElement GetElement(By by, String name, String type, String place) throws Exception {
        return GetElement(by, name, type, place, null);
    }

    public static WebElement GetElement(By by, String name, String type, String place, WebElement father) throws Exception {
        WebElement element;
        String messageSuccess = "Found - Element: '" + name + "', Type: '" + type + "', In: '" + place + "'";
        String messageError = "Not Found - Element: '" + name + "', Type: '" + type + "', In: '" + place + "'";

        try {
            if (father != null)
                element = father.findElement(by);
            else
                element = driver.findElement(by);
            Log.info(messageSuccess);
        } catch (Exception e) {
            Log.error(messageError);
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw (e);
        }
        return element;
    }

    //</editor-fold>

    //<editor-fold desc="Windows Methods">

    public static void GoTo(String url) throws Exception {
        try {
            Log.info("Executing Goto method...");
            String fullUrl = Constant.baseURL + url;
            Log.info("Full URL: " + fullUrl);

            if (driver == null) {
                Log.error("Driver is null");
                throw new Exception("Driver is null");
            }

            driver.get(fullUrl);
            Log.info("Going to " + fullUrl);
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GoTo | Exception occurred while going to a page : "
                    + e.getMessage());
            throw new Exception();
        }
    }

    public static void GoTo() throws Exception {
        try {
            Log.info("Executing Goto method...");
            String fullUrl = Constant.baseURL;
            Log.info("Full URL: " + fullUrl);

            if (driver == null) {
                Log.error("Driver is null");
                throw new Exception("Driver is null");
            }

            driver.get(fullUrl);
            Log.info("Going to " + fullUrl);
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GoTo | Exception occurred while going to a page : "
                    + e.getMessage());
            throw new Exception();
        }
    }


    public static void DisplayWindow(WebElement button, String buttonName, By window) throws Exception {
        try {
            GUtils.Pause(3000);
            GUtils.ClickButton(button, buttonName);
            // Check if the Dialog is displayed
            if (!GUtils.ExistsElement(window, false)) {
                Thread.sleep(1000);
                GUtils.ClickButton(button); // If the Add dialog does not appear, click add button again
                Log.info("Click action is performed on '" + buttonName + "' button by second time");
                GUtils.waitForAllElementsLocatedBy(window);
            }
        } catch (Exception e) {
            Log.error("Class GUtils | Method DisplayWindow | Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static void scrollToElement(By locator) {
        Locatable scrollToItem = (Locatable) driver.findElement(locator);
        int y = scrollToItem.getCoordinates().inViewPort().getY();

        jsExecutor.executeScript("window.scrollBy(0," + y + ");");
    }

    public static void scrollToElement(WebElement wElement) {
        Locatable scrollToItem = (Locatable) wElement;
        int y = scrollToItem.getCoordinates().inViewPort().getY();

        jsExecutor.executeScript("window.scrollBy(0," + y + ");");
    }

    public static void moveToElementByXpath(String xpath) throws Exception {
        try {
            WebElement mapObject = driver.findElement(By.xpath(xpath));
            jsExecutor.executeScript("arguments[0].click();", mapObject);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static void moveToElementByCssSelector(String selector) throws Exception {
        try {
            WebElement mapObject = driver.findElement(By.cssSelector(selector));
            jsExecutor.executeScript("arguments[0].click();", mapObject);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static void moveToElement(WebElement wElement) throws Exception {
        try {
            jsExecutor.executeScript("arguments[0].click();", wElement);
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static void moveToElementByActions(By locator) throws Exception {
        try {
            WebElement element = driver.findElement(locator);
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static void moveToElementByActions(WebElement wElement) throws Exception {
        try {
            Actions actions = new Actions(driver);
            actions.moveToElement(wElement);
            actions.perform();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static Boolean switchBetweenWindows() throws Exception {
        try {
            Set listOfWindows = driver.getWindowHandles();
            if (listOfWindows.size() != 2) {
                if (listOfWindows.size() > 2) {
                    throw new TooManyWindowsException();
                } else {
                    throw new TooFewWindowsException();
                }
            }
            String currentWindow = driver.getWindowHandle();
            for (Iterator i = listOfWindows.iterator(); i.hasNext(); ) {
                String selectedWindowHandle = i.next().toString();
                if (!selectedWindowHandle.equals(currentWindow)) {
                    driver.switchTo().window(selectedWindowHandle);
                    return true;
                }
            }
            // Just in case something goes wrong
            throw new UnableToFindWindowException("Unable to switch windows!");

        } catch (Exception ex) {
            Log.error(ex.getMessage());
            return null;
        }
    }

    public static Boolean switchToWindowTitled(String windowTitle) throws Exception {
        try {
            driver.switchTo().window(windowTitle);
            return true;
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            return null;
        }
    }

    public static void maximize() {
        driver.manage().window().maximize();
    }

    public static boolean isAlertPresent() throws Exception {
        boolean alertPresent = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constant.Waiting_Seconds);
            wait.until(ExpectedConditions.alertIsPresent());
            driver.switchTo().alert();
            alertPresent = true;
        } catch (Exception Ex) {
            Log.error(Ex.getMessage());
            throw new Exception(Ex.getMessage());
        }
        return alertPresent;
    }

    public static String getPageTitle() throws Exception {
        String title = "";
        try {
            title = driver.getTitle();
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            return null;
        }
        return title;
    }

    public static String getWindowURL() throws Exception {
        String url;
        try {
            url = driver.getCurrentUrl();
            return url;
        } catch (Exception ex) {
            Log.error(ex.getMessage());
            return null;
        }
    }

    public static void closeBrowser() throws Exception {
        try {
            driver.close();
            scenario = null;
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    public static void getBackInBrowser() throws Exception {
        driver.navigate().back();
    }

    public static void getForwardInBrowser() throws Exception {
        driver.navigate().forward();
    }

    public static void deleteAllCookies() throws Exception {
        driver.manage().deleteAllCookies();
    }

    //</editor-fold>

    //<editor-fold desc="Java Script Methods">

    public static void ExecuteJS(String jsCode) throws Exception {
        try {
            jsExecutor.executeScript(jsCode);

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method ExecuteJS | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    //</editor-fold>

    //<editor-fold desc="Utils Methods">

    public static String generateTimestamp() throws Exception {
        try {
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            String value = timestamp.toString().replace("-", "");
            value = value.replace(":", "");
            value = value.replace(".", "");
            value = value.replace(" ", "");

            return value;
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method generateTimestamp | Exception occurred: Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static void Pause(int timeToSleep) throws Exception {
        try {
            Thread.sleep(timeToSleep);
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method Pause | Exception occurred: Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static String getTestCaseName(String sTestCase) throws Exception {
        String value = sTestCase;
        try {
            int posi = value.indexOf("@");
            value = value.substring(0, posi);
            posi = value.lastIndexOf(".");
            value = value.substring(posi + 1);
            return value;
        } catch (Exception e) {
            Log.error("Class GUtils | Method getTestCaseName | Exception desc : "
                    + e.getMessage());
            throw (e);
        }
    }

    public static void captureScreenShot(WebDriver driver, String methodName) throws Exception {
        //logger.info("Take Screenshot");
        DateFormat dfn = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat df = new SimpleDateFormat("MMM . dd . yyyy _HH:mm:ss");

        String formattedDate = df.format(new Date());
        String folderName = dfn.format(new Date());

        //ScreenShots will be saved in different levels in folders, i.e, /#YEAR/#MONTH/#DAY.jpg
        formattedDate = formattedDate.replace(" ", "");
        formattedDate = formattedDate.replace(":", "");
        String filePath = getScreenShotsPath() + folderName.replace("/", "") + "/" + "_" + methodName + "_" + formattedDate + ".jpg";

        File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenShot, new File(filePath));
        Log.info("ScreenShot generated: " + filePath);
    }

    public static byte[] getScreenShotToEmbed() throws Exception {
        try {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            return screenshot;
        } catch (Exception e) {
            Log.error("Class GUtils | Method " + new Object() {
            }.getClass().getEnclosingMethod().getName() + " | Exception: " + e.getMessage());
            throw e;
        }
    }

    private static String getScreenShotsPath() throws Exception {

        try {
            return properties.get(0).getProperty("screenShotsFolder");
        } catch (Exception e) {
            Log.error("Class GUtils | Method " + new Object() {
            }.getClass().getEnclosingMethod().getName() + " | Exception: " + e.getMessage());
            throw e;
        }
    }

    //</editor-fold>

    //<editor-fold desc="Grid Methods">

    public static WebElement GetToolbarButton(String tooltip, String place) throws Exception {
        return GUtils.GetElement(By.xpath("//div[@title='" + tooltip + "']"), tooltip, "Grid Toolbar Button", place);
    }

    public static Boolean ExistsValueInGrid(String gridName) throws Exception {
        String script = "";
        int count = -1;
        try {
            script = "return $('" + gridName + "').grid.getRowsNum();";
            count = ((Long) jsExecutor.executeScript(script))
                    .intValue();
            Log.info("There are " + count + " rows in the Grid '" + gridName
                    + "'");

            return count > 0;

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method ExistsValueInGrid | Exception occurred while executing javascript. Code: "
                    + script + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static int GetColumnIndex(String gridName, String columnHeader) throws Exception {
        String jsCode = "";
        try {
            jsCode = " var columnHeader = '" + columnHeader + "'; "
                    + " var mygrid = $('" + gridName + "').grid; "
                    + " for (var i = 0; i < mygrid.getColumnsNum(); i++) {  "
                    + " var colLabel = mygrid.getColLabel(i).trim();  "
                    + " if( colLabel.toUpperCase() == columnHeader.toUpperCase() ) { "
                    + " return i;  "
                    + " }  "
                    + " }  "
                    + " return -1;";
            int columnIndex = ((Long) jsExecutor.executeScript(jsCode)).intValue();
            if (columnIndex < 0) {
                throw new Exception("Column " + columnHeader + " not found");
            }

            return columnIndex;
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetColumnIndex | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static WebElement GetTxtFilter(String gridName, String columnHeader) throws Exception {
        String jsCode = "";
        int index = GetColumnIndex(gridName, columnHeader);

        try {
            jsCode = "return $('" + gridName + "').grid.getFilterElement(" + index + ");";
            WebElement filter = (WebElement) jsExecutor.executeScript(jsCode);
            if (filter == null) {
                throw new Exception("Filter textbox is not found. Grid: " + gridName + ", ColumnHeader: " + columnHeader);
            }
            return filter;
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetTxtFilter | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static WebElement GetGridSelectedRowCell(String gridName, String columnHeader, int moveFromIndex) throws Exception {
        String jsCode = "";
        try {

            int columnIndex = GetColumnIndex(gridName, columnHeader) + moveFromIndex;

            jsCode = "return $('" + gridName + "').grid.cells($('" + gridName + "').grid.getSelectedRowId()," + columnIndex + ").cell; ";
            WebElement cell = (WebElement) jsExecutor.executeScript(jsCode);
            return cell;

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetGridCellValue | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static String GetGridCellValue(String gridName, String columnHeader, int moveFromIndex) throws Exception {
        String jsCode = "";
        try {

            int columnIndex = GetColumnIndex(gridName, columnHeader) + moveFromIndex;

            jsCode = "return $('" + gridName + "').grid.cells($('" + gridName + "').grid.getSelectedRowId()," + columnIndex + ").getValue(); ";
            String value = (String) jsExecutor.executeScript(jsCode);
            return value;

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetGridCellValue | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static int GetGridRowNumber(String gridName) throws Exception {
        int count = -1;
        String jsCode = "return $('" + gridName + "').grid.getRowsNum();";
        try {
            count = ((Long) jsExecutor.executeScript(jsCode)).intValue();
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetGridRowNumber | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
        return count;
    }

    public static Boolean ExistRowID(String gridName, String rowID) throws Exception {
        Boolean founded = false;
        String jsCode = "return $('" + gridName + "').grid.doesRowExist('" + rowID + "');";
        try {
            founded = (Boolean) jsExecutor.executeScript(jsCode);
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetGridSelectedRowID | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
        return founded;
    }

    public static String GetGridSelectedRowID(String gridName) throws Exception {
        String rowID = "";
        String jsCode = "return $('" + gridName + "').grid.getSelectedId();";
        try {
            rowID = (String) jsExecutor.executeScript(jsCode);
        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method GetGridSelectedRowID | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
        return rowID;
    }

    public static int LookForRowIndex(int rowNumber, String gridName, Filter[] filters) throws Exception {
        int rowIndex = -1;
        String jsCode = "";
        String ifCode = "";
        try {
            // Concat the if statement
            for (int i = 0; i < filters.length; i++) {

                int colIndex = GetColumnIndex(gridName, filters[i].columnHeader);
                String dataValue = filters[i].dataFieldName != null ? GExcelUtils.getCellData(rowNumber, filters[i].dataFieldName) : filters[i].value;

                if (ifCode != "")
                    ifCode += " && ";

                ifCode += "mygrid.cells(rowID," + colIndex + ").getValue().toString().indexOf('" + dataValue + "') > -1";
            }

            // CreateContractTerm the if statement against each grid row
            jsCode = " var mygrid = $('" + gridName + "').grid; "
                    + " var id = '';"
                    + " mygrid.forEachRow(function (rowID) {  "
                    + " if(id == ''){ "
                    + " if( " + ifCode + " ) { "
                    + " id = rowID;  "
                    + " }  "
                    + " }});  "
                    + " return id;";

            String rowID = (String) jsExecutor.executeScript(jsCode);


            jsCode = "return $('" + gridName + "').grid.getRowIndex('" + rowID + "');";
            rowIndex = ((Long) jsExecutor.executeScript(jsCode)).intValue();

            // Get the rowIndex

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method LookForRowIndex | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }

        return rowIndex;
    }

    public static void SelectGridRow(String gridName, int rowIndex) throws Exception {
        // http://docs.dhtmlx.com/api__dhtmlxgrid_selectrow.html
        String jsCode = "";
        try {
            jsCode = "$('" + gridName + "').grid.selectRow(" + rowIndex + ",true,false,true);";
            jsExecutor.executeScript(jsCode);
            Log.info("Row index " + rowIndex + " is selected in Grid '" + gridName + "'");

        } catch (Exception e) {
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            Log.error("Class GUtils | Method SelectGridRow | Exception occurred while executing javascript. Code: "
                    + jsCode + "   Exception: " + e.getMessage());
            throw new Exception();
        }
    }

    public static Boolean SelectGridRow(int rowNumber, String gridName, Filter[] filters) throws Exception {
        Boolean founded = false;
        int rowIndex = -1;
        try {
            Thread.sleep(1000); // Wait for loading message to be showed
            GUtils.waitForInvisibilityOfElementLocated(By.id(gridName + "Load"));

            rowIndex = GUtils.LookForRowIndex(rowNumber, gridName, filters);
            if (rowIndex >= 0) {
                GUtils.SelectGridRow(gridName, rowIndex);
                founded = true;
            } else {
                // If the entry is not in the first page, let's filter the grid.
                for (int i = 0; i < filters.length; i++) {
                    // Filter the Grid
                    if (filters[i].moveTo == 0) {
                        if (filters[i].dataFieldName != null && !filters[i].dataFieldName.isEmpty()) {
                            // Get the value from the test case data file
                            GUtils.SendKeys(rowNumber, GUtils.GetTxtFilter(gridName, filters[i].columnHeader), filters[i].dataFieldName);
                        } else if (!filters[i].value.isEmpty()) {
                            GUtils.SendKeys(GUtils.GetTxtFilter(gridName, filters[i].columnHeader), filters[i].columnHeader, filters[i].value);
                        }
                    }
                }
                Thread.sleep(1000);  // Wait for loading message to be showed
                GUtils.waitForInvisibilityOfElementLocated(By.id(gridName + "Load"));
                rowIndex = GUtils.LookForRowIndex(rowNumber, gridName, filters);
                if (rowIndex >= 0) {
                    GUtils.SelectGridRow(gridName, rowIndex);
                    founded = true;
                }
            }

            return founded;
        } catch (Exception e) {
            Log.error("Class GUtils | Method SelectGridRow(String gridName, Filter[] filters) | Exception: " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw new Exception();
        }
    }

    //</editor-fold>

    //<editor-fold desc="Mouse Hover Over Methods">

    public static void mouseHoverOver(WebDriver driver, WebElement hoverOverElement, WebElement revealedElement) throws Exception {
        try {

            Actions action = new Actions(driver);
            action.moveToElement(hoverOverElement).moveToElement(revealedElement).click().build().perform();
        } catch (Exception e) {
            Log.error("Class GUtils | Method performMouseOverClick(WebDriver driver, WebElement baseElement, WebElement overElement) | Exception: " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw new Exception();
        }
    }

    //</editor-fold>

    public static String convertLocalDateToString(LocalDate date) throws Exception {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.dateFormat);
            return date.format(formatter);

        } catch (Exception e) {
            Log.error("Class GUtils | Method dateFormated() | Exception: " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw new Exception();
        }
    }

    public static LocalDate convertStringToLocalDate(String date) throws Exception {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.dateFormat);
            return LocalDate.parse(date, formatter);

        } catch (Exception e) {
            Log.error("Class GUtils | Method convertStringToLocalDate() | Exception: " + e.getMessage());
            captureScreenShot(driver, new Exception().getStackTrace()[0].getMethodName());
            throw new Exception();
        }
    }

}