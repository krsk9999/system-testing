package com.gft.qa.automation.core.reporting;

import com.gft.qa.automation.core.utils.GUtils;
import com.gft.qa.automation.core.utils.Init;
import com.gft.qa.automation.core.utils.Log;
import org.testng.IExecutionListener;

public class TestNGExecutionListener implements IExecutionListener {
    @Override
    public void onExecutionStart() {

        try {
            Log.info("Setting Up Test Suite.");
            Init.initEnvironmentSettings();//Setting up system properties and environment variables

            GUtils.OpenBrowser();

        } catch (Exception error) {
            Log.error("Error Found while Setting up environment");
            Log.error(error.getMessage());
        }
    }

    @Override
    public void onExecutionFinish() {

        try {

            Log.info("Closing Test Suite.");
            GUtils.closeBrowser();
            Reporting.generateReport();

        } catch (Exception error) {
            Log.error("Error Found while Cleaning Up environment");
            Log.error(error.getMessage());
        }
    }
}
